# Breast size quiz

Quiz: *Who* has the largest **breasts**?

This program takes images of three naked woman, where one has significantly larger breasts than the other two.  
It shuffles those images around, covers up the nude parts, and gives you the covered up SFW image.
Can you guess which one of the three woman has the largest breasts?

![Example breast quiz](/img/quiz1.png "Example breast quiz")  
What do you think? 🍆,🍑 or 🌽?  
[answer (nsfw)](/img/quiz1_result.png)  

Live demonstration: start the [Telegram bot](https://t.me/BreastQuizBot) and send `/generate`.

Fun for in group chats.  
Send the covered image first, let people guess and then post the uncovered image as answer.

What the program does:

* it takes three nude images as input
* an AI determines the nude regions
* the images are put side to side
* a rectangle gets drawn which covers all the nipples
* emoji are put on the nipples for you to identify your choice
* star emojis are drawn on top of the genitalia

## Setup

Instructions to run the code yourself.

### Setup Python environment

A virtual environment is not required, but is good practice.  
Create virtual Python environment called `venv`:

```shell
python -m venv venv
```

Activate the created environment:

```shell
. venv/bin/activate
```

Install required python packages:

```shell
pip install -r requirements.txt
```

### Fix NudeNet library

The NudeNet library cannot properly download its neural network anymore.
To fix it, see what files it tries to download, download those manually and give them the same name:

* Run 'NudeDetector.py' found in [test](/test) folder
* In the exception you see the error originated from NudeNets 'detector.py'
* Within this `detector.py` you can find the download URLs and destination paths
* It tries to download the model file
  from `https://github.com/notAI-tech/NudeNet/releases/download/v0/detector_v2_default_checkpoint.onnx` and the classes
  file from `https://github.com/notAI-tech/NudeNet/releases/download/v0/detector_v2_default_classes`
* Download those files manually and rename the model file to `detector_v2_default_checkpoint.onnx` and the classes file
  to `classes`
* Put the files in `~/.NudeNet`, where those files already should be, but too small (28.1kb in my case, should be like
  100MB)
* `NudeDetector.py` should now work flawlessy

### Setup database

The project uses MongoDB to store scraped nudes and generated quizzes.
Install MongoDB and its GUI:

```shell
yay -Syu mongodb mongodb-compass
```

Enable MongoDB daemon:

```shell
sudo systemctl enable --now mongodb
```

Set MongoDB user and password:

```
use admin
db.createUser(
  {
    user: "USERNAME_HERE",
    pwd: "PASSWORD_HERE",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)
```

Update `etc/mongodb.conf` to enable security:

* Uncomment `# security`
* Append `authorization: "enabled"` under `security` and indent once
* Update `etc/mongodb.conf` to enable connections from any IP (remote connections):
* At `# network interfaces`, change the `bindIp` from `127.0.0.1` to `0.0.0.0`

Test the database connection in MongoDB Compass, and save the generated connection string in a file
called `mongodb_connection_string.txt` in the [private](/private) folder.

### Fill database

The database should be filled with information scraped from [boobpedia.com](https://boobpedia.com/boobs/Main_Page).
The scraper for this and its documentation can be found in the [boobpediascraper](/boobpediascraper) folder.

## Usage

### CLI

The Command Line Interface takes paths of nude images as input:

```shell
./QuizGenerator.py img/nude1.jpg img/nude2.jpg img/test_nude.jpg
```

This outputs the paths of the quiz image and the quiz answer image:

```shell
/tmp/1661968842714.png /tmp/1661968842714-raw.png
```

### Telegram bot

To use the Telegram bot, put your bot token in `telegram_bot_token.txt` in the [private](/private) folder.
Fill these files with your Telegram bot API token and your mongodb server connection string. Launch the Telegram bot:

```shell
./BreastQuizTelegramBot
```

Now images can be sent to the bot. To generate a quiz, send `/generate` to the bot.  
The `/generate` command takes the last three send images as input by default. To change this, pass a number as argument,
for example: `/generate 2`.  
For other commands and more info, run the `/help` command.

#### Run Telegram bot on startup
* Install `cronie` and `byobu`
* Run `crontab -e`
* Put `@reboot byobu new-session -d -s "scripts" "/home/jort/this/repos/breast-size-quiz/venv/bin/python /home/jort/this/repos/breast-size-quiz/BreastQuizTelegramBot"`


## Code layout

### Database

Messages send and retrieved are stored in the MongoDB database:
| number | message type |
|---|---|
| 0 | retrieved compressed image |
| 1 | retrieved image |
| 2 | send quiz image|
| 3 | send quiz results |
| 4 | send image |
| 5 | retrieved message |
| 6 | send message |

## Notes

* Should also work for male nudes, but not tested.
* Only tested on Arch Linux, Ubuntu or Windows users may have to launch the python scripts different.

## Possible improvements

* Store nude points in db instead of determining them live
* Don't draw genitals if already covered by rectangle

## Credits

Notable libraries used:

* NudeNet: https://github.com/notAI-tech/NudeNet
* OpenCV: https://github.com/opencv/opencv-python
* pyTelegramBotAPI: https://github.com/eternnoir/pyTelegramBotAPI








