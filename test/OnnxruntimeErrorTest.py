from nudenet import NudeDetector
import time
import datetime

timers = {}


def start_timer(name):
    timers[name] = datetime.datetime.now()


def get_elapsed(name):
    if name not in timers.keys():
        print(f"{name} is not an existing timer.")
        return 1
    time_diff = datetime.datetime.now() - timers[name]
    return int(time_diff.total_seconds() * 1000)


for x in range(10):
    detector = NudeDetector()  # i need to initialize this each time to prevent errors :(
    start_timer(x)
    print(f"Detecting {x}")
    detections = detector.detect("../img/failed_nude.jpg", mode="fast")
    print(f"{get_elapsed(x)}ms: {detections}")
