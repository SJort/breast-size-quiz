import matplotlib.pyplot as plt
import numpy as np
import requests
from io import BytesIO
from PIL import Image
from cairosvg import svg2png
import cv2


def generate_rect(x, y, radius, margin_left=0):
    x_min = x - radius + margin_left
    y_min = y - radius
    x_max = x + radius + margin_left
    y_max = y + radius
    return x_min, y_min, x_max, y_max


def overlay_image_alpha(img, img_overlay, x, y, alpha_mask):
    # Taken from: https://stackoverflow.com/questions/14063070/overlay-a-smaller-image-on-a-larger-image-python-opencv
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return
    img_crop = img[y1:y2, x1:x2]
    img_overlay_crop = img_overlay[y1o:y2o, x1o:x2o]
    alpha = alpha_mask[y1o:y2o, x1o:x2o, np.newaxis]
    alpha_inv = 1.0 - alpha
    img_crop[:] = alpha * img_overlay_crop + alpha_inv * img_crop


def paste_emoji(x, y, scale, img, emoji_path):
    emoji_svg = open(emoji_path)
    emoji_png = svg2png(file_obj=emoji_svg, scale=scale)
    emoji_pil = Image.open(BytesIO(emoji_png)).convert('RGBA')

    # align to center
    emoji_cv2 = cv2.cvtColor(np.array(emoji_pil), cv2.COLOR_RGBA2BGRA)  # to cv2
    delta_x = emoji_cv2.shape[1] // 2
    delta_y = emoji_cv2.shape[0] // 2
    x -= delta_x
    y -= delta_y

    # cv2 image to pil
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = np.array(Image.fromarray(img))
    emoji_rgba = np.array(emoji_pil)

    # blend images
    alpha_mask = emoji_rgba[:, :, 3] / 255.0
    img_result = img[:, :, :3].copy()
    img_overlay = emoji_rgba[:, :, :3]
    overlay_image_alpha(img_result, img_overlay, x, y, alpha_mask)

    # pil to cv2
    img = cv2.cvtColor(np.array(img_result), cv2.COLOR_RGBA2BGRA)
    return img


img_path = "../img/eerie.jpg"
emoji_path = "../emoji-svg/1-eggplant.svg"
img = cv2.imread(img_path)

x = img.shape[1] // 2
y = img.shape[0] // 2

x_min, y_min, x_max, y_max = generate_rect(x, y, 10)
cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (255, 0, 0), -1)
# x = 50
# y = 50
img = paste_emoji(x=x, y=y, img=img, scale=10, emoji_path=emoji_path)
plt.imshow(img)
plt.show()
