
test = "hoi dit (is (mooi) haha) hoi"

def after(string, part):
    return string.split(part, 1)[1]

def before(string, part):
    string = string[::-1]
    string = string.split(part, 1)[1]
    return string[::-1]

def between(string, part1, part2):
    return before(after(string, part1), part2)

r1 = after(test, "(")
r2 = before(test, ")")
r3 = between(test, "(", ")")
print(f"r1: {r1}")

print(f"r2: {r2}")
print(f"r3: {r3}")
