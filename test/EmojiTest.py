import emoji
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import os

im = Image.open("img/eerie.jpg")
draw = ImageDraw.Draw(im)
path = "font/NotoEmoji-Regular.ttf"
font = ImageFont.truetype(path, 64, encoding='unic')
est = "Hello 😀"
textToDraw = emoji.emojize("Hello :eggplant:")
draw.text((20,20), textToDraw, (255,255,255), font=font)
im.show()
