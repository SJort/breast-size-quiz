"""
Scrapes pages with natural breasts from Boobpedia, and saves them to database.
"""
import datetime
import os
import sys
import time

import pymongo
import urllib3
from bs4 import BeautifulSoup

http = urllib3.PoolManager()

# database initialization
CONNECTION_STRING = open(os.path.join(sys.path[0], "../private/mongodb_connection_string.txt"), "r").read().strip()
db_client = pymongo.MongoClient(CONNECTION_STRING)
db = db_client["breastquiz"]
nude_page_collection = db["nude_page"]

# determine last downloaded page
d = {}
last_document = nude_page_collection.find_one(d, sort=[("_id", pymongo.DESCENDING)])
last_page = last_document["url"] if last_document is not None else None

# determine page to start scraping from
domain = "https://www.boobpedia.com"
next_page = domain + "/boobs/Category:Natural_boobs"
if last_page is not None:
    next_page = last_page
print(f"Start scraping from {next_page}")


def download_html(url):
    # download html page from url and return the html text
    global http
    response = http.request("GET", url)
    response_text = response.data.decode("utf-8")
    return response_text


def save_to_database(url, html):
    # save downloaded html string to database with its url
    d = {"url": url,
         "content": html,
         "time": datetime.datetime.utcnow(),
         }
    nude_page_collection.insert_one(d)


def find_url_to_next_page(html):
    # find url to next page in the html, return None if not found
    soup = BeautifulSoup(html, "html.parser")
    result = soup.find(name="a", string="next page")
    if result is None:
        return None
    result = result.get("href")
    if result is None:
        return None
    result = domain + result
    return result

# start scraping
while True:
    print(f"Downloading {next_page}")
    html = download_html(next_page)
    save_to_database(next_page, html)
    next_page = find_url_to_next_page(html)
    if next_page is None:
        print(f"Could not find URL to next page.")
        break
    time.sleep(0.1)

print("Done! Check the last URL to see if it's really the last.")