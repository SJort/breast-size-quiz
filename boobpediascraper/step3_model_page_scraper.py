"""
Scrapes the pages of the models from the downloaded pages with pagescraper, and saves them to database.
"""
import unicodedata
from html import unescape

import pymongo
import urllib3
from bs4 import BeautifulSoup

from cup_size_converter import *
from timer import *

http = urllib3.PoolManager()

# database initialization
CONNECTION_STRING = open(os.path.join(sys.path[0], "../private/mongodb_connection_string.txt"), "r").read().strip()
db_client = pymongo.MongoClient(CONNECTION_STRING)
db = db_client["breastquiz"]
model_page_collection = db["model_page"]
model_collection = db["model"]

# already scraped pages
already_scraped = model_collection.find({}, {"url": 1})
already_scraped_urls = [x["url"] for x in already_scraped]

base_url = "https://www.boobpedia.com"


def find_in_info_rows(info_rows, attribute):
    # find the value for a key in the right info table
    for info_row in info_rows:
        if attribute in info_row.text:
            res = info_row.find_all("td")[1].text
            return res


def find_in_info_rows_exception_wrapper(info_rows, attribute):
    try:
        return find_in_info_rows(info_rows, attribute).strip()
    except:
        return None


def after(string, part):
    # get everything after the first match of 'part' in a string
    try:
        return string.split(part, 1)[1]
    except:
        # print(f"After failed on '{part}' with '{string}', returned whole string.")
        pass
    return string


def before(string, part):
    # get everything before the first match of 'part' in a string
    try:
        s = string[::-1]  # otherwise returned reversed
        s = s.split(part, 1)[1]
        return s[::-1]
    except:
        # print(f"Before failed on '{part}' with '{string}', returned whole string.")
        pass
    return string


def between(string, part1, part2):
    # 'hi (haha) hey (yoo)', '(', ')' -> returns 'haha) hey (yoo'
    return before(after(string, part1), part2)


amount_parsed = 0


def parse(document):
    # parses attributes from a model page
    # example page: https://www.boobpedia.com/boobs/19Honeysuckle
    # skips mandatory values, other values which can't be found are set to None

    global amount_parsed
    amount_parsed += 1

    url = document["url"]
    start_timer(url)
    print(f"Parsing {url}")
    if url in already_scraped_urls:
        print(f"Already parsed.")
        return

    html = document["content"]

    # remove &nbsp;
    html = unescape(html)
    html = unicodedata.normalize("NFKD", html)

    # skip empty pages
    if "There is no article with this exact name" in html:
        return

    soup = BeautifulSoup(html, "html.parser")

    info_table = soup.find("tbody")  # the right table with all the stats
    info_rows = info_table.find_all("tr", valign="top")  # rows in the stat table

    # name
    try:
        name = info_table.find("big").text
    except:
        return

    # age
    try:
        born = find_in_info_rows(info_rows, "Born")
        born = before(between(born, "(", ")"), ")")
        born = [int(x) for x in born.split("-")]
        born_year = born[0] if len(born) >= 1 else None
        born_month = born[1] if len(born) >= 2 else None
        born_day = born[2] if len(born) >= 3 else None
    except:
        born_year = None
        born_month = None
        born_day = None

    # breast sizes
    try:
        size = find_in_info_rows(info_rows, "Bra/cup")
        size = before(size, "(")
        size = [x.strip() for x in size.split(" ")]
        size_us = size[0]
        size_metric = between(size[1], "(", ")").strip()
        if size_metric == "":
            size_metric = None
        breast_weight = cup_size_to_kg(size_us)
    except:
        return

    # image source
    try:
        image = info_table.find(class_="image")
        image = after(str(image), 'srcset="')  # get by attribute does not seem to work
        image = image.split()
        image_url = base_url + image[0]
    except:
        return

    # height
    try:
        height = find_in_info_rows(info_rows, "Height")
        height = between(height, "(", ")")
        height = before(height, "m").strip()
        height = float(height)
    except:
        height = None

    # other features
    ethnicity = find_in_info_rows_exception_wrapper(info_rows, "Ethnicity")
    nationality = find_in_info_rows_exception_wrapper(info_rows, "Nationality")
    breast_type = find_in_info_rows_exception_wrapper(info_rows, "Boobs")
    body_type = find_in_info_rows_exception_wrapper(info_rows, "Body")
    eye_color = find_in_info_rows_exception_wrapper(info_rows, "Eye")
    hair_color = find_in_info_rows_exception_wrapper(info_rows, "Hair")
    pubic_hair = find_in_info_rows_exception_wrapper(info_rows, "Pubic")

    d = {
        # copy the first 2 values from origin collection
        "url": url,
        "time": document["time"],

        "name": name,
        "image_url": image_url,
        "born_year": born_year,
        "born_month": born_month,
        "born_day": born_day,
        "ethnicity": ethnicity,
        "nationality": nationality,
        "size_us": size_us,
        "size_metric": size_metric,
        "breast_weight": breast_weight,
        "breast_type": breast_type,
        "height": height,
        "body_type": body_type,
        "eye_color": eye_color,
        "hair_color": hair_color,
        "pubic_hair": pubic_hair,
    }

    d = dict((k, v.strip() if isinstance(v, str) else v) for k, v in d.items())  # strip all string values in dict
    model_collection.insert_one(d)  # will give duplicate documents if run again
    average = get_elapsed("runtime") // amount_parsed
    print(f"Found #{amount_parsed} in {get_elapsed(url)}ms ({average}): {d}")


# start scraping
# document_cursor = model_page_collection.find({}, limit=100)
document_cursor = model_page_collection.find({})

start_timer("runtime")
for document in document_cursor:
    parse(document)

print(f"Done extracting!")
