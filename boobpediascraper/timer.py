"""
Class for setting timers, used for debugging and logging.
"""
import datetime

# dict for keeping all the timer start values in
timers = {}


def start_timer(name):
    # start a timer with a name
    timers[name] = datetime.datetime.now()


def get_elapsed(name):
    # get the elapsed time in ms since start for a timer with a name
    if name not in timers.keys():
        print(f"{name} is not an existing timer.")
        return 1
    time_diff = datetime.datetime.now() - timers[name]
    return int(time_diff.total_seconds() * 1000)
