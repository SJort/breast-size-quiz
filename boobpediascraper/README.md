# Boobpedia scraper
Scrapes information of models with natural breasts from [boobpedia.com](https://boobpedia.com/boobs/Main_Page) and saves results in the MongoDB database.

# Usage
Run the scripts in the order below.  
I have chosen to split the scraping into multiple scripts, and save the intermediary results into the database.  
This puts less strain on the [boobpedia.com](https://boobpedia.com/boobs/Main_Page) servers, 
separates the code, and if I decide to scrape more information from a models page for example, 
I can so faster with the downloaded pages.

## Step 1 
* Downloads all the pages containing list of links to natural breast models
* Example of downloaded page: https://www.boobpedia.com/wiki/index.php?title=Category:Natural_boobs&pagefrom=Agatha+Meirelles#mw-pages
* Saves the HTML to `nude_page` collection

## Step 2
* Downloads the model pages themselves using the links in the downloaded HTML found in `nude_page` collection
* Example of downloaded page: https://www.boobpedia.com/boobs/Alessa_Snow  
* Saves the HTML to `model_page` collection 

## Step 3
* Extracts model information from all the model pages found in the `model_page` collection
* Example of page where information is extracted: https://www.boobpedia.com/boobs/19Honeysuckle
* Extracts the image link, bra size, length, weight and more information from the right table
* The extracted model details are saved to the `model` collection 

## Step 4
* Download the images from the extracted model data stored in the `model` collection
* Example of downloaded image link: https://www.boobpedia.com/wiki/images/d/d9/Alessa_Snow_01.jpg
* Saves the downloaded image directly as bytes back into the `model` collection

## Step 5
* Analyses the model data found in the `model` collection
* Checks if the image contains enough nude parts
* Converts breast size
* Marks documents as valid if the data is usable for the breast size quiz


