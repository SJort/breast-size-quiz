import os
import sys

"""
Read the CSV file to a list of lists
remove the tabs
separate the top row with the column names
"""
_bra_sizes_path = os.path.join(sys.path[0], "bra_sizes.csv")
_data_rows = []
_column_names = None
with open(_bra_sizes_path) as file:
    for line in file:
        items = []
        for item in line.split(","):
            items.append(item.strip())
        _data_rows.append(items)
_column_names = _data_rows.pop(0)
_cup_size_index = _column_names.index("bra-size-us")
_uk_cup_size_index = _column_names.index("bra-size-uk")
_breast_kg_index = _column_names.index("joined-weight-kg")

"""
the CSV has all the cup sizes within one value, which is not easy to handle 
convert all the cup sizes within that value to a list and make them lowercase
"""
for _row in _data_rows:
    cup_size_list = []
    for cup_size in _row[_cup_size_index].split(" "):
        cup_size_list.append(cup_size.strip().lower())
    _row[_cup_size_index] = cup_size_list

    uk_cup_size_list = []
    for cup_size in _row[_uk_cup_size_index].split(" "):
        uk_cup_size_list.append(cup_size.strip().lower())
    _row[_uk_cup_size_index] = uk_cup_size_list


def cup_size_to_kg(cup):
    for data_row in _data_rows:
        breast_kg = float(data_row[_breast_kg_index])
        cup_sizes = data_row[_cup_size_index]
        if cup.lower() in cup_sizes:
            return breast_kg
    return None


if __name__ == "__main__":
    _cups_to_test = []
    _cups_to_test.append(["40A", 1.1])
    _cups_to_test.append(["40AA", None])
    _cups_to_test.append(["42d", 2.1])
    _cups_to_test.append(["e", None])
    _cups_to_test.append(["spaghetti", None])
    _cups_to_test.append(["32P", 5.3])

    for cup_to_test in _cups_to_test:
        cup = cup_to_test[0]
        weight = cup_size_to_kg(cup)
        should_be = cup_to_test[1]
        success = weight == should_be
        success_string = "success" if success else "FAILED"
        thingy = "" if success else f"and should be {should_be}"
        print(f"{success_string}: cup {cup} is {weight} {thingy}")



