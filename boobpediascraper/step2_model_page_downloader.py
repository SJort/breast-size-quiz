"""
Scrapes the pages of the models from the downloaded pages with pagescraper, and save them to database.
"""
import datetime
import os
import sys
import time
from multiprocessing.pool import ThreadPool

import pymongo
import urllib3
from bs4 import BeautifulSoup

http = urllib3.PoolManager()

# database initialization
CONNECTION_STRING = open(os.path.join(sys.path[0], "../private/mongodb_connection_string.txt"), "r").read().strip()
db_client = pymongo.MongoClient(CONNECTION_STRING)
db = db_client["breastquiz"]
nude_page_collection = db["nude_page"]
model_page_collection = db["model_page"]

domain = "https://www.boobpedia.com"


def download_html(url):
    # download html page from url and return the html text
    global http
    response = http.request("GET", url)
    response_text = response.data.decode("utf-8")
    return response_text


def save_to_database(url, html):
    # save downloaded html string to database with its url
    d = {"url": url,
         "content": html,
         "time": datetime.datetime.utcnow(),
         }
    model_page_collection.insert_one(d)


def get_model_page_urls(html):
    # find all urls of model pages
    soup = BeautifulSoup(html, "html.parser")
    page_list = soup.find(id="mw-pages")
    page_list = page_list.find(class_="mw-content-ltr")
    page_list_links = page_list.find_all("a")
    return page_list_links

def download_model_page(url):
    # download a single page of a model
    url = url.get("href")
    url = domain + url

    # ignore urls of pages of lists
    if "List_of" in url:
        return

    # skip urls already downloaded (in database)
    if url in already_downloaded_urls:
        return

    print(f" - {url}")
    html = download_html(url)
    save_to_database(url, html)


# get already downloaded urls from database to avoid redownloading
already_downloaded_documents = model_page_collection.find({}, {"url": 1})
already_downloaded_urls = [x["url"] for x in already_downloaded_documents]

# start scraping
documents = nude_page_collection.find({})
for document in documents:  # for all the pages containing links to model pages
    page_url = document["url"]
    print(f"Parsing page: {page_url}")
    html = document["content"]
    model_page_urls = get_model_page_urls(html)

    amount_of_threads = 10
    print(f"Start download model pages with {amount_of_threads} threads.")
    pool = ThreadPool(amount_of_threads)
    results = pool.map(download_model_page, model_page_urls)


print(f"Done downloading!")
