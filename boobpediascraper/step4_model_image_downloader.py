"""
Download images from the urls extracted with the model page extractor and save them to the database.
"""

import os
import sys
from multiprocessing.pool import ThreadPool
from timer import *

import pymongo

# database initialization
import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

CONNECTION_STRING = open(os.path.join(sys.path[0], "../private/mongodb_connection_string.txt"), "r").read().strip()
db_client = pymongo.MongoClient(CONNECTION_STRING)
db = db_client["breastquiz"]
model_collection = db["model"]

amount_parsed = 0
amount_skipped = 0


def parse(document):
    global amount_parsed
    url = document["image_url"]

    amount_parsed += 1
    start_timer(url)

    print(f"Downloading {url}")

    try:
        r = requests.get(url, allow_redirects=True, verify=False, timeout=30)
    except:
        print(f"ERROR: could not download {url}")
        return
    d = {"image": r.content}
    model_collection.update_one({"image_url": url}, {"$set": d})

    average = get_elapsed("runtime") // amount_parsed
    print(f"Downloaded #{amount_parsed} {url} in {get_elapsed(url)}ms (avg: {average}ms)")


# start scraping
document_cursor = model_collection.find({"$and": [{"image_url": {"$exists": True}}, {"image": {"$exists": False}}]})
print(f"Converting database to list...")
start_timer("convert")
document_list = list(document_cursor)  # takes about 5ms / record, needs to be in list for threadpool
print(f"Converted in {get_elapsed('convert')}ms.")
print(f"Iterating {len(document_list)} documents.")

amount_of_threads = 36
print(f"Start scraping model pages with {amount_of_threads} threads.")
start_timer("runtime")
pool = ThreadPool(amount_of_threads)
results = pool.map(parse, document_list)

print(f"Done extracting in {get_elapsed('runtime')}ms!")
