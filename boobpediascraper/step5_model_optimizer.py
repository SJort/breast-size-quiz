"""
Analyse downloaded images in the model collection for nude parts.

Reset validity:
mongosh -u USERNAME
use breastquiz
db.model.updateMany({}, {$unset: {valid_for_quiz:""}})
"""
import os
import sys
import tempfile
import time
import traceback

import cv2
import pymongo
from nudenet import NudeDetector

from timer import *

# database initialization

CONNECTION_STRING = open(os.path.join(sys.path[0], "../private/mongodb_connection_string.txt"), "r").read().strip()
VALID_FIELD = "valid_for_quiz"
db_client = pymongo.MongoClient(CONNECTION_STRING)
db = db_client["breastquiz"]
model_collection = db["model"]

required_width = 300
required_height = 300

# nude detector

amount_parsed = 0


def has_required_labels(labels):
    labels = set(labels)

    # visible face
    req = {"FACE_F", "FACE_M"}
    intersection = labels.intersection(req)
    if len(intersection) == 0:
        return False

    # visible breasts
    req = {"COVERED_BREAST_F", "EXPOSED_BREAST_F", "EXPOSED_BREAST_M", "EXPOSED_BREAST_F", "EXPOSED_BREAST_M"}
    intersection = labels.intersection(req)
    if len(intersection) == 0:
        return False

    # visible genitalia
    req = {"COVERED_GENITALIA_F", "EXPOSED_GENITALIA_F", "EXPOSED_GENITALIA_M", "EXPOSED_GENITALIA_F",
           "EXPOSED_GENITALIA_M"}
    intersection = labels.intersection(req)
    if len(intersection) == 0:
        return False

    return True


def parse(document):
    try:
        global amount_parsed
        amount_parsed += 1

        url = document["url"]
        start_timer(url)

        base_filename = round(time.time() * 1000)  # the count is not unique when multithreaded
        image_path = os.path.join(tempfile.gettempdir(), f"{amount_parsed}-{base_filename}.png")
        image_url = document["image_url"]

        has_image = False
        has_high_res_image = False
        has_required_nudity = False
        if "image" in document:
            print(f"Analysing {url} at {image_path} image: {image_url}")
            with open(image_path, "wb") as file:
                file.write(document["image"])

            raw_image = cv2.imread(image_path)
            width = raw_image.shape[1]
            height = raw_image.shape[0]
            if width > required_width and height > required_height:
                has_high_res_image = True

            try:
                detector = NudeDetector() # i need to keep initializing this to prevent errors
                detections = detector.detect(image_path,
                                             mode="fast")  # fast mode: negligible worse accuracy and 3x faster
                # Returns [{'box': LIST_OF_COORDINATES, 'score': PROBABILITY, 'label': LABEL}, ...]
                labels = [detection["label"] for detection in detections]
                has_required_nudity = has_required_labels(labels)
            except Exception as e:
                print(f"FAILED detecting: {e}")
            has_image = True

        has_breast_weight = "breast_weight" in document

        is_valid = has_image and has_high_res_image and has_required_nudity and has_breast_weight
        print(
            f"{'! IN' if not is_valid else ''}VALID {has_image=} {has_high_res_image=} {has_required_nudity=} {has_breast_weight=}")
        d = {VALID_FIELD: is_valid}

        average = get_elapsed("runtime") // amount_parsed

        model_collection.update_one({"url": url}, {"$set": d})
        print(f"Analysed #{amount_parsed} {url} in {get_elapsed(url)}ms ({average}): {d}")
    except Exception as e:
        print(f"ERROR analysing: {e}")
        traceback.print_exc()


# iterate all image which are not analysed and have an image to analyse
start_timer("runtime")
document_cursor = model_collection.find({"$and": [{VALID_FIELD: {"$exists": False}}, {"image": {"$exists": True}}]})
for document in document_cursor:
    parse(document)
print(f"Analysed in {get_elapsed('runtime')}ms.")

print(f"Done analysing!")
