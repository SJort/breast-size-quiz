"""
Edit values for each document in a collection.
Only use this script if you understand the code.
"""
import os
import sys

import pymongo
from cup_size_converter import *

CONNECTION_STRING = open(os.path.join(sys.path[0], "../private/mongodb_connection_string.txt"), "r").read().strip()
db_client = pymongo.MongoClient(CONNECTION_STRING)
db = db_client["breastquiz"]
model_collection = db["model"]
VALID_FIELD = "valid_for_quiz"
CUP_SIZE_FIELD = "size_us"
WEIGHT_FIELD = "breast_weight"
URL_FIELD = "url"

document_cursor = model_collection.find({VALID_FIELD: True},
                                        {URL_FIELD: 1, CUP_SIZE_FIELD: 1, WEIGHT_FIELD: 1, VALID_FIELD: 1})

count = 0
for document in document_cursor:
    cup_size = document[CUP_SIZE_FIELD]
    prev_breast_weight = document[WEIGHT_FIELD]
    breast_weight = cup_size_to_kg(cup_size)
    valid = document[VALID_FIELD]
    if prev_breast_weight != breast_weight:
        if breast_weight is None:
            valid = False
        after = "which is still valid" if valid else "which makes it invalid"
        print(f"Updating {cup_size} from {prev_breast_weight} to {breast_weight} {after}")
        d = {"breast_weight": breast_weight, VALID_FIELD: valid}
        model_collection.update_one({URL_FIELD: document[URL_FIELD]}, {"$set": d})
        count += 1

print(f"Done, updated {count} fields.")
