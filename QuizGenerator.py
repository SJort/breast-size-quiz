#!/usr/bin/env python
"""
Paste nude images of three paths next to another, determine the position of the breasts, and censor them all
with one rectangle.

- make all images same height
- detect nude parts
- concat images
- draw bounding rectangle of all breasts
- draw genitals
- draw emojis
"""
import os
import sys
import time
import traceback
import cv2
import argparse
import numpy as np
import tempfile
from io import BytesIO
from PIL import Image
from cairosvg import svg2png
from nudenet import NudeDetector

args = None


# debug / verbose print function
def vprint(*rgs, **kwrgs):
    if args is not None and not args.verbose:  # if args are defined as not verbose, skip print
        return
    print(" ".join(map(str, rgs)), **kwrgs)


emoji_folder = os.path.join(sys.path[0], "emoji-svg/")
emoji_paths = []
for filename in sorted(os.listdir(emoji_folder)):
    full_emoji_path = os.path.join(emoji_folder, filename)
    emoji_paths.append(full_emoji_path)


def generate_rect(x, y, radius, margin_left=0):
    x_min = x - radius + margin_left
    y_min = y - radius
    x_max = x + radius + margin_left
    y_max = y + radius
    return x_min, y_min, x_max, y_max


def overlay_image_alpha(img, img_overlay, x, y, alpha_mask):
    # Taken from: https://stackoverflow.com/questions/14063070/overlay-a-smaller-image-on-a-larger-image-python-opencv
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return
    img_crop = img[y1:y2, x1:x2]
    img_overlay_crop = img_overlay[y1o:y2o, x1o:x2o]
    alpha = alpha_mask[y1o:y2o, x1o:x2o, np.newaxis]
    alpha_inv = 1.0 - alpha
    img_crop[:] = alpha * img_overlay_crop + alpha_inv * img_crop


def paste_emoji(x, y, scale, img, emoji_path):
    emoji_svg = open(emoji_path)
    emoji_png = svg2png(file_obj=emoji_svg, scale=scale)
    emoji_pil = Image.open(BytesIO(emoji_png)).convert('RGBA')

    # align to center
    emoji_cv2 = cv2.cvtColor(np.array(emoji_pil), cv2.COLOR_RGBA2BGRA)  # to cv2
    delta_x = emoji_cv2.shape[1] // 2
    delta_y = emoji_cv2.shape[0] // 2
    x -= delta_x
    y -= delta_y

    # cv2 image to pil
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = np.array(Image.fromarray(img))
    emoji_rgba = np.array(emoji_pil)

    # blend images
    alpha_mask = emoji_rgba[:, :, 3] / 255.0
    img_result = img[:, :, :3].copy()
    img_overlay = emoji_rgba[:, :, :3]
    overlay_image_alpha(img_result, img_overlay, x, y, alpha_mask)

    # pil to cv2
    img = cv2.cvtColor(np.array(img_result), cv2.COLOR_RGBA2BGRA)
    return img


def image_resize(image, width=None, height=None, inter=cv2.INTER_LANCZOS4):
    # only pass the max height OR the max width
    # taken from stackoverflow

    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))
    resized = cv2.resize(image, dim, interpolation=inter)
    return resized


def determine_regions(path):
    vprint(f"Processing nude regions of {path}.")
    image = cv2.imread(path)
    detector = NudeDetector()
    results = detector.detect(path, mode='fast')  # fast mode: negligible worse accuracy and 3x faster
    # Returns [{'box': LIST_OF_COORDINATES, 'score': PROBABILITY, 'label': LABEL}, ...]
    vprint(f"Processing results.")

    # calculate bounding box of both breasts
    x_min = 100000
    x_max = 0
    y_min = 100000
    y_max = 0
    breast_detections = [detection for detection in results if detection["label"] == "EXPOSED_BREAST_F"]
    for breast_detection in breast_detections:
        vprint(breast_detection)
        coords = breast_detection["box"]
        x1 = coords[0]
        y1 = coords[1]
        x2 = coords[2]
        y2 = coords[3]
        x_min = min(x_min, x1, x2)
        x_max = max(x_max, x1, x2)
        y_min = min(y_min, y1, y2)
        y_max = max(y_max, y1, y2)

    # center of joined breast bounding box
    center_x = (x_max + x_min) // 2
    center_y = (y_max + y_min) // 2

    result = {
        "path": path,
        "x_min": x_min,
        "y_min": y_min,
        "x_max": x_max,
        "y_max": y_max,
        "center_x": center_x,
        "center_y": center_y,
        "image_width": image.shape[1],  # just for convenience (dirty)
    }

    # center of genital bounding box
    genitalia = [detection for detection in results if detection["label"] == "EXPOSED_GENITALIA_F"]
    if len(genitalia) != 0:
        genitalia = genitalia[0]
        coords = genitalia["box"]
        x1 = coords[0]
        y1 = coords[1]
        x2 = coords[2]
        y2 = coords[3]
        genitalia_center_x = (x2 + x1) // 2
        genitalia_center_y = (y2 + y1) // 2
        result["genitalia_x"] = genitalia_center_x
        result["genitalia_y"] = genitalia_center_y
    else:
        vprint(f"WARNING: no genitalia detected.")
    return result


def generate_quiz(paths):
    """
    - make all images same height
    - detect nude parts
    - concat images
    - draw bounding rectangle of all breasts
    - draw genitals
    - draw emojis
    """

    # output names
    filename = round(time.time() * 1000)
    folder = tempfile.gettempdir()
    result_path = os.path.join(folder, f"{filename}.png")
    raw_result_path = os.path.join(folder, f"{filename}-raw.png")

    # determine minimal x and y dimensions of all images
    images = []
    min_width = 10000
    min_height = 10000
    for image_path in paths:
        raw_image = cv2.imread(image_path)
        width = raw_image.shape[1]
        height = raw_image.shape[0]
        min_width = min(min_width, width)
        min_height = min(min_height, height)
        vprint(f"Raw image '{image_path}' dimensions: {width}x{height}.")
        images.append(raw_image)

    vprint(f"Minimal dimensions: {min_width}x{min_height}")

    # resized all images to minimal dimensions
    images = [image_resize(img, width=min_width, height=min_height) for img in images]
    resized_images = []
    for image in images:
        resized_image = image_resize(image, height=min_height)
        vprint(f"Resized to {resized_image.shape[1]}x{resized_image.shape[0]}")
        resized_images.append(resized_image)

    # concat resized images
    vprint(f"Concatenating images.")
    merged_image = cv2.hconcat(resized_images)
    vprint(f"Concatenated images into one of dimensions {merged_image.shape[1]}x{merged_image.shape[0]}.")

    # saved concatenated unedited result
    vprint(f"Saving unedited result.")
    cv2.imwrite(raw_result_path, merged_image)

    # save each individual resized image (library can only read paths)
    vprint(f"Saving resized images.")
    resized_paths = []
    count = 0

    filename = round(time.time() * 1000)
    for resized_image in resized_images:
        resized_path = os.path.join(folder, f"resized-{count}-{filename}.jpg")
        count += 1
        cv2.imwrite(resized_path, resized_image)
        resized_paths.append(resized_path)

    # determine nude regions of resized images
    vprint(f"Determining nude regions of resized images.")
    regions_list = []
    for resized_path in resized_paths:
        regions = determine_regions(resized_path)
        regions_list.append(regions)
        vprint(f"Found regions: {regions}")

    # determine bounding box of all detected breasts
    vprint(f"Determining shared bounding box of breasts.")
    x_min = 10000
    x_max = 0
    y_min = 10000
    y_max = 0
    for regions in regions_list:
        x1 = regions["x_min"]
        y1 = regions["y_min"]
        x2 = regions["x_max"]
        y2 = regions["y_max"]
        x_min = min(x_min, x1)
        y_min = min(y_min, y1)
        x_max = max(x_max, x2)
        y_max = max(y_max, y2)

    # draw bounding box
    vprint(f"Drawing shared bounding box.")
    bounding_box_width = merged_image.shape[1]
    bounding_box_height = merged_image.shape[0]
    y_max += int(bounding_box_height * 0.07)  # little buffer otherwise you can tell
    cv2.rectangle(merged_image, (0, y_min), (bounding_box_width, y_max), (255, 153, 51), -1)

    # draw choice marker emoji
    vprint(f"Drawing emoji choice markers.")
    total_left_margin = 0
    index = 0
    for regions in regions_list:
        nude_width = regions["image_width"]
        x_min = total_left_margin
        x_max = total_left_margin + nude_width
        # takes the y_values from previous bounding box calculations
        center_x = (x_max + x_min) // 2
        center_y = (y_max + y_min) // 2

        emoji_path = emoji_paths[index]
        index += 1

        merged_image = paste_emoji(x=center_x, y=center_y, img=merged_image, scale=5, emoji_path=emoji_path)

        total_left_margin += nude_width

    # draw genitalia
    total_left_margin = 0
    for regions in regions_list:
        width = regions["image_width"]
        if "genitalia_x" not in regions:
            vprint(f"Skipping undetected genitals.")
            total_left_margin += width  # always do this, otherwise everything shifted
            continue

        x, y = regions["genitalia_x"], regions["genitalia_y"]
        x = x + total_left_margin
        radius = int(merged_image.shape[0] * 0.03)
        vprint(f"Drawing genitalia area {x},{y} with margin {total_left_margin} with radius {radius}")
        merged_image = paste_emoji(x=x, y=y, img=merged_image, scale=2, emoji_path=emoji_paths[9])
        total_left_margin += width

    cv2.imwrite(result_path, merged_image)
    return result_path, raw_result_path


def is_valid_path(string):
    if os.path.isfile(string):
        return string
    else:
        print(f"Not a file: {string}")
        quit(1)


if __name__ == "__main__":
    description = "Horizontally concatenates passed images and covers nude parts " \
                  "and puts a emoji in each of the rectangles covering the breast areas."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("image_paths", type=is_valid_path, nargs="+", help="paths to images to use")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-o", "--open_result", help="opens the generated files",
                        action="store_true")
    args = parser.parse_args()
    vprint(f"Using paths: {args.image_paths}")
    paths = [path for path in args.image_paths]

    try:
        result, raw_result = generate_quiz(paths)
        print(result, raw_result)
        if args.open_result:
            print(f"Showing results")
            cv2.imshow("Quiz result", cv2.imread(result))
            try:
                while True:  # the loop allows killing from the terminal
                    cv2.waitKey(1)  # press any key in preview to close
            except KeyboardInterrupt:
                pass
        quit(0)
    except Exception as e:
        print(traceback.format_exc())
        print(f"Error generating: {e}")
        quit(1)
